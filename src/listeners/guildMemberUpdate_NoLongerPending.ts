import { SettingsEnum } from '@prisma/client';
import { Listener, PieceContext } from '@sapphire/framework';
import { Constants, GuildMember } from 'discord.js';
import { validateMember } from '../lib/validate-member';

export class UserEvent extends Listener {
	constructor(context: PieceContext) {
		super(context, {
			event: Constants.Events.GUILD_MEMBER_UPDATE
		});
	}

	public async run(beforeMember: GuildMember, afterMember: GuildMember) {
		const validatedRoleId = await this.container.configurator.get(SettingsEnum.VALIDATED_ROLE_ID);

		if (beforeMember.pending && !afterMember.pending && !afterMember.roles.cache.some((role) => role.id === validatedRoleId)) {
			const logsChannelId = await this.container.configurator.get(SettingsEnum.LOGS_CHANNEL_ID);
			const logsChannel = await beforeMember.guild.channels.fetch(logsChannelId);

			if (logsChannel === null || !logsChannel.isText()) {
				return this.container.logger.error(
					`Erreur lors de la récupération du channel de logs. L'id enregistré est \`${logsChannelId}\`. Son settings est \`${SettingsEnum.LOGS_CHANNEL_ID}\``
				);
			}

			logsChannel.send(`${afterMember} member is no longer pending. Let's check if they are for more than 10mins too.`);

			if (afterMember.joinedTimestamp !== null && new Date().getTime() - afterMember.joinedTimestamp >= 10 * 60 * 1000) {
				logsChannel.send({
					content: `${afterMember} is no longer pending AND has joined 10mins, let's validate them!`
				});
				validateMember(afterMember, this.container, logsChannel);
			}
		}
	}
}
