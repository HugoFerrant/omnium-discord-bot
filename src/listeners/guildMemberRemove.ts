import { Listener } from '@sapphire/framework';
import type { GuildMember } from 'discord.js';
import { SettingsEnum } from '../lib/settings/settings.enum';

export class UserEvent extends Listener {
	public async run(member: GuildMember) {
		try {
			const logsChannelId = await this.container.configurator.get(SettingsEnum.LOGS_CHANNEL_ID);
			const logsChannel = await member.guild.channels.fetch(logsChannelId);

			if (member.joinedTimestamp === null) {
				return; // should never happend
			}

			if (logsChannel === null || !logsChannel.isText()) {
				return this.container.logger.error(
					`Erreur lors de la récupération du channel de logs. L'id enregistré est \`${logsChannelId}\`. Son settings est \`${SettingsEnum.LOGS_CHANNEL_ID}\``
				);
			}

			if (Date.now() - member.joinedTimestamp <= 10 * 60 * 1000) {
				logsChannel.send(
					`<@${member.id}> a insta-leave (en ${Math.floor((Date.now() - member.joinedTimestamp) / 1000)}secondes, belle perf!).`
				);
			} else {
				logsChannel.send(`<@${member.id}> a décidé de s'en aller vers un meilleur monde. Adieu mon frère.`);
			}
		} catch (error) {
			this.container.logger.error("Erreur lors de l'envoie du message de leave : " + error);
		}
	}
}
