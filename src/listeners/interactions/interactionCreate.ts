import { Interaction, ButtonInteraction, Constants } from 'discord.js';
import { Listener } from '@sapphire/framework';
import type { PieceContext } from '@sapphire/pieces';
import { ButtonHandler } from '../../lib/buttons-handling/button-handler.abstract';

export default class InteractionCreate extends Listener<typeof Constants.Events.INTERACTION_CREATE> {
	constructor(context: PieceContext) {
		super(context, {
			event: Constants.Events.INTERACTION_CREATE
		});
	}

	public async run(interaction: Interaction): Promise<void> {
		if (interaction.isCommand()) {
			this.container.logger.debug("Command interaction created. Let's ignore that for now and let Sapphire handle it");
		} else if (interaction.isButton()) {
			this.container.logger.debug("Button interaction created. Let's handle it");
			this.buttonInteractionHandler(interaction);
		} else if (interaction.isAutocomplete()) {
			this.container.logger.debug(`Autocomplete interaction created for command ${interaction.commandName}. Let's handle it`);
			//todo: better autocomplete interaction handling
			if (interaction.commandName === 'validate') {
				const focusedValue = interaction.options.getFocused(true);
				const members = (await interaction.guild!.members.fetch({ query: String(focusedValue.value) })) ?? [];
				const filtered = members.filter((member) => member.roles.cache.size === 1); //@everyone count as a role

				interaction.respond(filtered.map((member) => ({ name: member.nickname ?? member.displayName, value: member.id })));
			}
		}
	}

	private async buttonInteractionHandler(buttonInteraction: ButtonInteraction): Promise<void> {
		const buttonInteractionIdentifier = ButtonHandler.parseIdentifierFromCustomId(buttonInteraction.customId);

		const buttonHandlersStore = this.container.stores.get('button-handlers');

		const matchingButtonHandler = buttonHandlersStore.find((buttonHandler) => buttonHandler.identifier === buttonInteractionIdentifier);
		if (matchingButtonHandler) {
			matchingButtonHandler.handle(buttonInteraction);
		}
	}
}
