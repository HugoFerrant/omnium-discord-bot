import { Listener } from '@sapphire/framework';
import type { GuildMember } from 'discord.js';
import { SettingsEnum } from '../lib/settings/settings.enum';
import { validateMember } from '../lib/validate-member';

export class UserEvent extends Listener {
	public async run(member: GuildMember) {
		const logsChannelId = await this.container.configurator.get(SettingsEnum.LOGS_CHANNEL_ID);
		const logsChannel = await member.guild.channels.fetch(logsChannelId);

		if (logsChannel === null || !logsChannel.isText()) {
			return this.container.logger.error(
				`Erreur lors de la récupération du channel de logs. L'id enregistré est \`${logsChannelId}\`. Son settings est \`${SettingsEnum.LOGS_CHANNEL_ID}\``
			);
		}

		logsChannel.send(
			`${member} vient d'arriver sur le serveur. ` +
				'\nSi vous le connaissez, vous pouvez le valider avec un clic-droit sur son pseudo, puis `Application`>`validate via user` (ou bien avec la commande de la plèbe: `/validate`).' +
				'\nDans le cas contraire, il suivra le process habituel de 10mins+règles.'
		);

		new Promise((resolve) => setTimeout(resolve, 1000 * 60 * 10)) //verification level
			.then(async () => {
				if (!member.guild.members.fetch(member.user)) {
					logsChannel.send(`L'utilisateur ${member} a leave avant d'être validé, donc pas de bienvenue.`);
					return;
				}

				logsChannel.send(
					`It's been 10mins since ${member} has joined. Let's check if they are no longer pending too (if not, it'll be handled by the NoLongerPending event).`
				);
				if (!member.pending) {
					logsChannel.send({
						content: `${member} has joined 10mins ago AND is no longer pending , let's validate them!`
					});
					validateMember(member, this.container, logsChannel);
				}
			});
	}
}
