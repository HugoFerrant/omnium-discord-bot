import { PrismaClient } from '@prisma/client';

export function createDatabaseConnection() {
	return new PrismaClient();
}
