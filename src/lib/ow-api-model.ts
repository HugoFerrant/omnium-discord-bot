export interface Awards {
	cards: number;
	medals: number;
	medalsBronze: number;
	medalsSilver: number;
	medalsGold: number;
}

export interface Games {
	played: number;
	won: number;
}

export interface CompetitiveStats {
	awards: Awards;
	games: Games;
}

export interface Awards2 {
	cards: number;
	medals: number;
	medalsBronze: number;
	medalsSilver: number;
	medalsGold: number;
}

export interface Games2 {
	played: number;
	won: number;
}

export interface QuickPlayStats {
	awards: Awards2;
	games: Games2;
}

export interface Rating {
	level: number;
	role: RoleEnum;
	roleIcon: string;
	rankIcon: string;
}

export enum RoleEnum {
	TANK = 'tank',
	DAMAGE = 'damage',
	SUPPORT = 'support'
}

export enum RegionEnum {
	EU = 'eu',
	US = 'us',
	ASIA = 'asia'
}

export enum PlatformEnum {
	PC = 'pc',
	PSN = 'psn',
	XBL = 'xbl',
	SWITCH = 'nintendo-switch'
}

export interface ProfileData {
	error: undefined; //helps to discriminate the union type below
	competitiveStats: CompetitiveStats;
	endorsement: number;
	endorsementIcon: string;
	gamesWon: number;
	icon: string;
	level: number;
	levelIcon: string;
	name: string;
	prestige: number;
	prestigeIcon: string;
	private: boolean;
	quickPlayStats: QuickPlayStats;
	rating: number;
	ratingIcon: string;
	ratings: Rating[];
}

export interface ErrorData {
	error: string;
}

export type ApiData = ProfileData | ErrorData;
