import { Piece } from '@sapphire/framework';
import type { ButtonInteraction } from 'discord.js';

const buttonCustomIdSeparator = '|';

export abstract class ButtonHandler extends Piece {
	public abstract identifier: string;
	public abstract handle(interaction: ButtonInteraction): Promise<void>;

	public buildCustomIdFromData(data: string) {
		return this.identifier + buttonCustomIdSeparator + data;
	}

	public parseDataFromCustomId(customId: string | null) {
		if (customId === null) return null;

		const identifierAndSeparatorIndex = customId.indexOf(this.identifier + buttonCustomIdSeparator);

		if (identifierAndSeparatorIndex === -1) return null;

		return customId.slice((this.identifier + buttonCustomIdSeparator).length);
	}

	public static parseIdentifierFromCustomId(customId: string | null) {
		if (customId === null) return null;

		return customId.split(buttonCustomIdSeparator).shift();
	}
}
