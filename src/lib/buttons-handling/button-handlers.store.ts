import { Store } from '@sapphire/framework';
import { join } from 'path';
import { ButtonHandler } from './button-handler.abstract';

const storeName = 'button-handlers';

export class ButtonHandlersStore extends Store<ButtonHandler> {
	public constructor() {
		super(ButtonHandler as any, { name: storeName, paths: [join(__dirname, 'button-handlers')] });
	}
}

declare module '@sapphire/pieces' {
	export interface StoreRegistryEntries {
		[storeName]: ButtonHandlersStore;
	}
}
