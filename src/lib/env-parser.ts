import { isNullishOrEmpty } from '@sapphire/utilities';

export function envParseArray(key: string, defaultValue?: string[]): string[] {
	const value = process.env[key];
	if (isNullishOrEmpty(value)) {
		if (defaultValue === undefined) throw new Error(`[ENV] ${key} - The key must be an array, but is empty or undefined.`);
		return defaultValue;
	}

	return value.split(' ');
}

//todo: clean up usage of this VS usage of SkyraEnv vs usage Kaname env plugin ?
