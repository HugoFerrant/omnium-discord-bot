import { Prisma, PrismaClient } from '@prisma/client';
import { DefaultSettings } from './default-settings';
import { SettingsNotFoundError } from './settings-not-found.error';
import type { SettingsEnum } from './settings.enum';

export class Configurator {
	constructor(private readonly database: PrismaClient) {}

	public async get(settingName: SettingsEnum) {
		try {
			return (await this.getOneSetting(settingName)).value;
		} catch (error) {
			if (error instanceof SettingsNotFoundError) {
				const defaultSetting = DefaultSettings[settingName];
				if (defaultSetting !== null) {
					return defaultSetting;
				}
			}
			throw error;
		}
	}
	public async getOneSetting(settingName: SettingsEnum) {
		const settingFound = await this.database.settings.findUnique({
			where: { name: settingName }
		});

		if (settingFound === null) {
			throw new SettingsNotFoundError(settingName);
		}

		return settingFound;
	}
	public async getAllSettings() {
		return await this.database.settings.findMany();
	}

	public async set(name: SettingsEnum, value: string, upsert: boolean = true) {
		if (!upsert) {
			try {
				return await this.database.settings.update({
					where: { name },
					data: { value }
				});
			} catch (error) {
				//cf https://github.com/prisma/prisma/issues/11344 for better error codes
				if (error instanceof Prisma.PrismaClientKnownRequestError && error.code === 'P2025') {
					// P2025 = record not found
					throw new SettingsNotFoundError(name);
				}
			}
		}

		return await this.database.settings.upsert({
			where: { name },
			create: { name, value },
			update: { value }
		});
	}

	public async unset(name: SettingsEnum) {
		await this.database.settings.delete({ where: { name } });
	}
}
