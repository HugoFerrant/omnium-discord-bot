import { SettingsEnum } from './settings.enum';

export function isSettingsEnum(token: string): token is SettingsEnum {
	return Object.values(SettingsEnum).includes(token as SettingsEnum);
}
