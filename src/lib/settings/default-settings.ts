import { SettingsEnum } from './settings.enum';

export const DefaultSettings: { [key in SettingsEnum]: string | null } = {
	[SettingsEnum.APRIL_FOOL_DOOMED_CATEGORY_ID]: null,
	[SettingsEnum.APRIL_FOOL_ROLES]: null,
	[SettingsEnum.APRIL_FOOL_ROLES_POSITION]: null,
	[SettingsEnum.LOGS_CHANNEL_ID]: null,
	[SettingsEnum.PREFIX]: null,
	[SettingsEnum.ROLE_ATTRIBUTION_CHANNEL_ID]: null,
	[SettingsEnum.ROLE_ATTRIBUTION_MESSAGE_ID]: null,
	[SettingsEnum.WELCOME_CHANNEL_ID]: null,
	[SettingsEnum.VALIDATED_ROLE_ID]: null
};
