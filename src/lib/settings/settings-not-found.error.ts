import type { SettingsEnum } from './settings.enum';
import { UserCommand as ConfigurationGetCommand } from '../../commands/General/Configuration/configuration-get';

export class SettingsNotFoundError extends Error {
	settingName: SettingsEnum;
	constructor(settingName: SettingsEnum) {
		super(`Le setting ${settingName} n'existe pas (voir la commande \`${ConfigurationGetCommand.name}\` pour avoir les settings).`);
		this.settingName = settingName;
	}
}
