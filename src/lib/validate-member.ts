import { DiscordAPIError, GuildMember, NewsChannel, TextChannel } from 'discord.js';
import type { Container } from '@sapphire/pieces';
import { SettingsEnum } from '@prisma/client';
import { RESTJSONErrorCodes } from 'discord-api-types/v9';
import { SettingsNotFoundError } from './settings/settings-not-found.error';
import { pickRandom } from './utils';

const welcomeMessages = [
	'**Wouhouwou** ! Une nouvelle tête ! Merci {{member}} de nous avoir rejoints, et bienvenue !' +
		"\nMaintenant que tu as accepté les règles, tu es libre d'écrire dans la _multitude_ de chans qui parsèment ce Discord." +
		" N'hésite pas à commencer en te présentant, si tu le souhaites, et sinon, à très vite pour quelques games endiablées <a:animercy_hype:794943396501520414>!",
	"Bonjour et bienvenue à toi, {{member}} ! Marche en paix dans l'Omnium. Tu peux commencer, si tu le souhaites, par te présenter." +
		"\nSi tu rencontres le moindre souci suite à ton arrivée sur l'Omnium, n'hésite pas à épingler un membre de l'administration !" +
		' On se retrouve bien vite en game <:ana_inlove:773216734819647529>',
	"_Boop_! {{member}} vient d'arriver en wallride sur le serveur!" +
		" Bienvenue sur l'Omnium! N'hésite pas à te présenter si tu le souhaites!" +
		'\nPenses aussi à faire un tour sur {{attributionsMessageChannel}} pour avoir un rôle et accéder à plus de channels et commencer à jouer avec nous!' +
		'\nAu plaisir de te revoir par ici! :wave:',
	"_BIP BIP BOOP BOOP_, comme dirait l'autre." +
		' Il veut probablement te souhaiter la bienvenue {{member}}. Promis, nos omniaques sont bien dressés.' +
		" Installe-toi tranquillement sur l'Omnium, et au plaisir de te croiser dans les chans ou en game !",
	"Je ne fais qu'un avec {{member}}." +
		" Bienvenue à toi dans l**'OMNIUM**." +
		" N'oublie pas de passer par le channel {{attributionsMessageChannel}} pour claim tes rôles <:widow_hi:773216045631799317>"
];

export async function validateMember(member: GuildMember, container: Container, logsChannel: NewsChannel | TextChannel): Promise<void> {
	try {
		const validatedRoleId = await container.configurator.get(SettingsEnum.VALIDATED_ROLE_ID);

		if (member.roles.cache.some((role) => role.id === validatedRoleId)) {
			logsChannel.send({
				content: `${member} is already validated ! (they have the <@&${validatedRoleId}> role)`,
				allowedMentions: { roles: [] }
			});

			return;
		}

		member.roles.add(validatedRoleId);

		const welcomeChannelId = await container.configurator.get(SettingsEnum.WELCOME_CHANNEL_ID);

		const attributionsChannelId = await container.configurator.get(SettingsEnum.ROLE_ATTRIBUTION_CHANNEL_ID);

		const welcomeChannel = await container.client.channels.fetch(welcomeChannelId);

		if (welcomeChannel?.type !== 'GUILD_TEXT') {
			logsChannel.send(`We could not say welcome to ${member}, because the welcome channel ${welcomeChannel} is not a text channel!`);
			return;
		}

		const welcomeMessage = pickRandom(welcomeMessages)
			.replace('{{member}}', member.toString())
			.replace('{{attributionsMessageChannel}}', `${attributionsChannelId}`);

		welcomeChannel.send(welcomeMessage);
	} catch (error) {
		if (error instanceof SettingsNotFoundError) {
			if (error.settingName === 'VALIDATED_ROLE_ID') {
				logsChannel.send(
					`${SettingsEnum.VALIDATED_ROLE_ID} setting is missing, so we can't give new members the role. Set it it with \`/configuration-set\`.`
				);
			} else if (error.settingName === 'WELCOME_CHANNEL_ID') {
				logsChannel.send(
					`${SettingsEnum.WELCOME_CHANNEL_ID} setting is missing, so we can't say welcome . Set it it with \`/configuration-set\`.`
				);
			}
		} else if (error instanceof DiscordAPIError && error.code === RESTJSONErrorCodes.UnknownRole) {
			logsChannel.send(`${SettingsEnum.VALIDATED_ROLE_ID} setting is wrong (role not found), so we can't give new members the role.`);
		} else {
			logsChannel.send(`Erreur inconnu lors de l'arrivée d'un nouveau membre : ${error}`);
		}
	}
}
