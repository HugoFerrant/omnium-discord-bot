import { send } from '@sapphire/plugin-editable-commands';
import { ColorResolvable, Constants, Message, MessageEmbed } from 'discord.js';
import { RandomLoadingMessage } from './constants';

/**
 * Picks a random item from an array
 * @param array The array to pick a random item from
 * @example
 * const randomEntry = pickRandom([1, 2, 3, 4]) // 1
 */
export function pickRandom<T>(array: readonly T[]): T {
	const { length } = array;
	return array[Math.floor(Math.random() * length)];
}

/**
 * Sends a loading message to the current channel
 * @param message The message data for which to send the loading message
 */
export function sendLoadingMessage(message: Message): Promise<typeof message> {
	return send(message, { embeds: [new MessageEmbed().setDescription(pickRandom(RandomLoadingMessage)).setColor('#FF0000')] });
}

export function dateToUnixTimestamp(date: Date): number {
	return Math.floor(date.getTime() / 1000);
}

export function isDiscordColorResolvable(color: any): color is ColorResolvable {
	return /^#(?:[0-9a-fA-F]{3}){1,2}$/.test(color) || Object.keys(Constants.Colors).includes(color);
}
