import { ButtonInteraction, CacheType, DiscordAPIError } from 'discord.js';
import { RESTJSONErrorCodes } from 'discord-api-types/v9';
import { ButtonHandler } from '../lib/buttons-handling/button-handler.abstract';

export class RoleAttributionButtonHandler extends ButtonHandler {
	public identifier = RoleAttributionButtonHandler.getIdentifier();

	public static getIdentifier() {
		return 'role-attribution';
	}

	public async handle(interaction: ButtonInteraction<CacheType>) {
		await interaction.deferReply({ ephemeral: true });
		const roleId = this.parseDataFromCustomId(interaction.customId);
		if (roleId === null) {
			this.container.logger.error('Role-attribution button clicked with no customId data');
			return;
		}

		if (interaction.inCachedGuild()) {
			try {
				if (interaction.member.roles.cache.find((role) => role.id === roleId)) {
					await interaction.member.roles.remove(roleId);
					interaction.editReply(`Félicitation, tu viens de perdre le rôle : <@&${roleId}>`);
				} else {
					await interaction.member.roles.add(roleId);
					interaction.editReply(`Félicitation, tu viens de gagner le rôle : <@&${roleId}>`);
				}
			} catch (error) {
				if (error instanceof DiscordAPIError && error.code === RESTJSONErrorCodes.MissingPermissions) {
					interaction.editReply(
						`**MissingPermissions error !** Le bot n'a probablement pas le droit d'ajouter/supprimer le rôle <@&${roleId}>.` +
							'\nVérifiez que le rôle du bot soit bien **au-dessus** de ce rôle dans la liste des rôles du serveur.'
					);
				} else if (error instanceof DiscordAPIError && error.code === RESTJSONErrorCodes.UnknownRole) {
					interaction.editReply(`**UnknownRole error !** Le role <@&${roleId}> a certainement été supprimé depuis qu'il a été ajouté ici.`);
				} else {
					this.container.logger.warn('Error with adding the role : ', error);
					interaction.editReply('Unknown error with adding/removing the role');
				}
			}
		} else {
			this.container.logger.warn('A button was clicked in a guild which is not cached :(');
		}
	}
}
