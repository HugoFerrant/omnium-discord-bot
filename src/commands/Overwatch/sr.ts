import fetch from 'node-fetch';
import { ColorResolvable, CommandInteraction, ContextMenuInteraction, Message, MessageEmbed, MessageOptions, Permissions } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { ApplicationCommandType } from 'discord-api-types/v9';
import { type CommandOptions, Command, Args, ArgumentError, Identifiers, ApplicationCommandRegistry, RegisterBehavior } from '@sapphire/framework';
import { ApiData as OwApiData, PlatformEnum, Rating, RegionEnum, RoleEnum } from '../../lib/ow-api-model';
import { envParseArray } from '../../lib/env-parser';

@ApplyOptions<CommandOptions>({
	aliases: ['rank'],
	description: 'Affiche le(s) rang(s) OW de la saison actuelle du joueur dont le btag est donné.',
	cooldownDelay: 5_000,
	cooldownFilteredUsers: envParseArray('OWNERS'),
	preconditions: [
		{
			name: 'UserPermissions',
			context: {
				permissions: new Permissions('SEND_MESSAGES')
			}
		}
	],
	detailedDescription:
		'Affiche le(s) rang(s) OW de la saison actuelle du joueur dont le btag est donné.' +
		"\nSi la carrière du joueur n'est pas en Public, le bot préviens l'utilisateur de la commande." +
		'\nPar défaut, la commande donne tous les rangs dont les placements  ont été faits.' +
		'\nLe paramètre optionnel Role a pour valeurs possibles : `tank`, `damage`, `support` (par défaut tous les rôles dispos sont donnés).' +
		`\nLe paramètre optionnel Region est par défaut sur \`${RegionEnum.EU}\` (autres valeurs possibles: \`${RegionEnum.US}\` et \`${RegionEnum.ASIA}\`).` +
		`\nLe paramètre optionnel Plateforme est par défaut sur \`${PlatformEnum.PC}\` (autres valeurs possibles: \`${PlatformEnum.PSN}\`, \`${PlatformEnum.XBL}\` et \`${PlatformEnum.SWITCH}\`).`
})
export class UserCommand extends Command {
	private readonly ROLE_COLORS = {
		[RoleEnum.TANK]: '#A8D7E9' as ColorResolvable,
		[RoleEnum.DAMAGE]: '#A895C7' as ColorResolvable,
		[RoleEnum.SUPPORT]: '#FBF8D4' as ColorResolvable
	};

	private readonly BATTLETAG_REGEX = /(^([A-zÀ-ú][A-zÀ-ú0-9]{2,11})|(^([а-яёА-ЯЁÀ-ú][а-яёА-ЯЁ0-9À-ú]{2,11})))(#[0-9]{4,})$/;

	private readonly OPTIONS_NAMES = {
		BATTLE_TAG: 'battle-tag',
		ROLE_TO_FILTER: 'role-to-filter',
		PLATFORM: 'platform',
		REGION: 'region'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.BATTLE_TAG)
							.setDescription('The battletag of the player you want.')
							.setRequired(true)
					)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.ROLE_TO_FILTER)
							.setDescription('The roleToFilter of the player you want (par défaut tous les rôles).')
							.setChoices(Object.entries(RoleEnum))
					)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.PLATFORM)
							.setDescription('The platform of the player you want (par défaut PC).')
							.setChoices(Object.entries(PlatformEnum))
					)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.REGION)
							.setDescription('The region of the player you want (par défaut EU).')
							.setChoices(Object.entries(RegionEnum))
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015267577405460'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
		registry.registerContextMenuCommand(
			(builder) =>
				builder //
					.setName(this.name + ' avec discord tag')
					.setType(ApplicationCommandType.User),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015268965736478'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
		registry.registerContextMenuCommand(
			(builder) =>
				builder //
					.setName(this.name + ' avec btag message')
					.setType(ApplicationCommandType.Message),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015270203031623'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async messageRun(message: Message, args: Args) {
		// mandatory args
		const battleTag = await args.pick('string').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return undefined;
			} else {
				throw error;
			}
		});

		if (battleTag === undefined) {
			return message.reply('Il manquerait pas le battletag souhaité par hasard :thinking: ?');
		}

		if (!battleTag.match(this.BATTLETAG_REGEX)) {
			return message.reply(`"${battleTag}" n'a pas l'air d'être un battletag valide petit cancrelat!`);
		}

		// optional args
		const parsingWarnings: string[] = [];

		const roleToFilter = (await args.pick('enum', { enum: Object.values(RoleEnum) }).catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return null;
			} else if (error.identifier === Identifiers.ArgumentEnumError) {
				parsingWarnings.push(`Erreur sur \`${error.parameter}\`: ${error.message}` + '\nTous les rôles dispos seront donc donnés.');
				args.pick('string'); //just to get through this wrong args
				return null;
			} else {
				throw error;
			}
		})) as RoleEnum | null;

		const region = (await args.pick('enum', { enum: Object.values(RegionEnum) }).catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return RegionEnum.EU;
			} else if (error.identifier === Identifiers.ArgumentEnumError) {
				parsingWarnings.push(`Erreur sur \`${error.parameter}\` : ${error.message}` + `\nRégion \`${RegionEnum.EU}\` par défaut.`);
				args.pick('string'); //just to get through this wrong args
				return RegionEnum.EU;
			} else {
				throw error;
			}
		})) as RegionEnum;

		const platform = (await args.pick('enum', { enum: Object.values(PlatformEnum) }).catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return PlatformEnum.PC;
			} else if (error.identifier === Identifiers.ArgumentEnumError) {
				parsingWarnings.push(`Erreur sur \`${error.parameter}\` : ${error.message}` + `\nPlateforme \`${PlatformEnum.PC}\` par défaut.`);
				return PlatformEnum.PC;
			} else {
				throw error;
			}
		})) as PlatformEnum;

		try {
			const replyMessageOptions = await this.doTheThing(battleTag, roleToFilter, platform, region);
			if (parsingWarnings.length !== 0) {
				replyMessageOptions.content = parsingWarnings.join('\n');
			}

			return message.reply(replyMessageOptions);
		} catch (error) {
			return message.reply(error as string);
		}
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		await interaction.deferReply(); // OW API is sometimes slow, we have to defer

		const battleTag = interaction.options.getString(this.OPTIONS_NAMES.BATTLE_TAG, true);
		const roleToFilter = interaction.options.getString(this.OPTIONS_NAMES.ROLE_TO_FILTER) as RoleEnum | null;
		const platform = interaction.options.getString(this.OPTIONS_NAMES.PLATFORM) as PlatformEnum | null;
		const region = interaction.options.getString(this.OPTIONS_NAMES.REGION) as RegionEnum | null;

		try {
			const replyMessageOptions = await this.doTheThing(battleTag, roleToFilter, platform ?? PlatformEnum.PC, region ?? RegionEnum.EU);

			return interaction.editReply(replyMessageOptions);
		} catch (error) {
			return interaction.editReply(error as string);
		}
	}

	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		await interaction.deferReply({ ephemeral: true }); // OW API is sometimes slow, we have to defer

		let battleTag;
		if (interaction.isUserContextMenu()) {
			battleTag = interaction.targetUser.tag;
		} else if (interaction.isMessageContextMenu()) {
			battleTag = interaction.targetMessage.content;
		} else {
			this.container.logger.error('Context menu interaction with an unhandled type (' + interaction.targetType + ')', interaction);
			return interaction.editReply('Erreur inconnue !');
		}

		try {
			if (!battleTag.match(this.BATTLETAG_REGEX)) {
				return interaction.editReply(`"${battleTag}" n'a pas l'air d'être un battletag valide petit cancrelat!`);
			}

			const replyMessageOptions = await this.doTheThing(battleTag, null, PlatformEnum.PC, RegionEnum.EU);

			return interaction.editReply(replyMessageOptions);
		} catch (error) {
			return interaction.editReply(error as string);
		}
	}

	// Zhu Li, do the THING !
	private async doTheThing(battleTag: string, roleToFilter: RoleEnum | null, platform: PlatformEnum, region: RegionEnum): Promise<MessageOptions> {
		const battleTagWithDash = battleTag.replace('#', '-');

		const apiResponse = await fetch(`https://ow-api.com/v1/stats/${platform}/${region}/${battleTagWithDash}/profile`);

		if (apiResponse.status === 404) {
			throw `Ce profil est introuvable! (${battleTag}, ${region}, ${platform})\nVérifiez par exemple les majuscules du battletag`;
		} else if (!apiResponse.ok) {
			this.container.logger.error(apiResponse);
			throw 'API indisponible, ré-essayez plus tard.';
		}

		const apiResponseData: OwApiData = await apiResponse.json();

		if (apiResponseData.error !== undefined) {
			this.container.logger.fatal(apiResponseData.error, apiResponse);
			throw `Erreur inconnue! Contactez ${envParseArray('OWNERS')
				.map((ownerId) => `<@${ownerId}>`)
				.join(', ')}`;
		}

		if (apiResponseData.private) {
			throw 'Ce profil est privé! Vous pouvez modifier ce paramètre en jeu dans Options > Social';
		}

		const embeds = [];
		let description =
			`[Lien playoverwatch.com (source des données)](https://playoverwatch.com/en-us/career/${platform}/${battleTagWithDash})` +
			`\n[Lien overbuff pour plus de détails](https://www.overbuff.com/players/pc/${battleTagWithDash})`;

		const buildEmbedFromRating = (rating: Rating) =>
			new MessageEmbed()
				.setColor(this.ROLE_COLORS[rating.role])
				.setTitle(String(rating.level))
				.setAuthor({ name: rating.role, iconURL: rating.roleIcon })
				.setThumbnail(rating.rankIcon);

		//should be null, not empty, but just in case
		if (apiResponseData.ratings === null || apiResponseData.ratings.length === 0) {
			description += '\n\n**Aucun placement dans aucun rôle pour ce joueur cette saison.**';
		} else {
			if (roleToFilter === null) {
				apiResponseData.ratings.forEach((rating) => {
					embeds.push(buildEmbedFromRating(rating));
				});
			} else {
				const ratingFound = apiResponseData.ratings.find((rating) => rating.role === roleToFilter);
				if (ratingFound === undefined) {
					description += `\n\n**Aucun placement dans ce rôle (${roleToFilter}) pour ce joueur cette saison.**`;
				} else {
					embeds.push(buildEmbedFromRating(ratingFound));
				}
			}
		}

		embeds.unshift(new MessageEmbed().setTitle(battleTag).setDescription(description));

		return { embeds };
	}
}
