import { ApplyOptions } from '@sapphire/decorators';
import { Args, Command, CommandOptions } from '@sapphire/framework';
import type { Message } from 'discord.js';

@ApplyOptions<CommandOptions>({
	description: 'Permet de recharger tout ou partie du bot',
	preconditions: ['OwnerOnly']
})
export class UserCommand extends Command {
	public constructor(context: Command.Context, options: Command.Options) {
		super(context, { ...options });
	}

	public async messageRun(message: Message, args: Args) {
		this.container.logger.debug(args.finished);
		//check Skyra's reload
		return message.reply(`todo`);
	}
}
