import { ApplyOptions } from '@sapphire/decorators';
import { Args, ArgumentError, Command, CommandOptions, Identifiers, ApplicationCommandRegistry, RegisterBehavior } from '@sapphire/framework';
import type { CommandInteraction, GuildTextBasedChannel, Message } from 'discord.js';
import { envParseArray } from '../../lib/env-parser';

@ApplyOptions<CommandOptions>({
	description: 'Envoie X messages.',
	preconditions: ['OwnerOnly']
})
export class UserCommand extends Command {
	private readonly OPTIONS_NAMES = {
		NUMBER_MESSAGES: 'number_messages',
		TARGET_CHANNEL: 'target_channel'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addNumberOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.NUMBER_MESSAGES)
							.setDescription('The battletag of the player you want')
							.setRequired(true)
					)
					.addChannelOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.TARGET_CHANNEL)
							.setDescription('The channel where to send the spam')
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015264997904434'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async messageRun(message: Message, args: Args) {
		if (message.channel.type !== 'GUILD_TEXT') {
			return message.reply('This command can only be used in guild text channels.');
		}
		const messageChannel = message.channel; //just for type guard because TS is dumb af

		const nbMessages = await args.pick('number').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return undefined;
			} else {
				throw error;
			}
		});

		if (nbMessages === undefined) {
			return message.reply('Il manquerait pas le nombre de messages souhaité par hasard :thinking: ?');
		}

		const targetChannel = await args.pick('guildTextChannel').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return messageChannel;
			} else {
				throw error;
			}
		});

		await this.doTheThing(nbMessages, targetChannel);

		return message.react('✅');
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		if (interaction.channel?.type !== 'GUILD_TEXT') {
			return interaction.reply({ content: 'This command can only be used in guild text channels.', ephemeral: true });
		}
		await interaction.deferReply({ ephemeral: true });

		const numberMessages = interaction.options.getNumber(this.OPTIONS_NAMES.NUMBER_MESSAGES, true);
		const targetChannel = interaction.options.getChannel(this.OPTIONS_NAMES.TARGET_CHANNEL);

		if (targetChannel && targetChannel?.type !== 'GUILD_TEXT') {
			return interaction.editReply('This command can only send messages in text channels.');
		}

		try {
			await this.doTheThing(numberMessages, targetChannel ?? interaction.channel);

			return interaction.editReply('Done');
		} catch (error) {
			return interaction.editReply(error as string);
		}
	}

	private async doTheThing(nbMessages: number, targetChannel: GuildTextBasedChannel) {
		for (let index = 1; index <= nbMessages; index++) {
			await targetChannel.send(`Spam Message ${index}/${nbMessages}`);
		}
	}
}
