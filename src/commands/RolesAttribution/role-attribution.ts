import { CommandInteraction, Guild, Message, MessageActionRow, MessageButton, Role } from 'discord.js';
import { ApplyOptions } from '@sapphire/decorators';
import { type CommandOptions, Command, ApplicationCommandRegistry, RegisterBehavior } from '@sapphire/framework';
import { InteractionLimits, MessageLimits } from '@sapphire/discord-utilities';
import { envParseArray } from '../../lib/env-parser';
import type { APIRole } from 'discord-api-types/v9';
import { SettingsEnum } from '../../lib/settings/settings.enum';
import { RoleAttributionButtonHandler } from '../../button-handlers/role-attribution.button-handler';
import { SettingsNotFoundError } from '../../lib/settings/settings-not-found.error';

@ApplyOptions<CommandOptions>({
	generateDashLessAliases: true,
	description: "Permet de gérer le message d'attribution des rôles",
	cooldownDelay: 5_000,
	cooldownFilteredUsers: envParseArray('OWNERS'),
	detailedDescription: '',
	preconditions: ['GuildOnly', ['OwnerOnly', 'AdminOnly']]
})
export class UserCommand extends Command {
	public override async messageRun(message: Message) {
		return message.reply("Désolé, mais cette commande n'est disponible qu'en mode slash-command (c'était trop relou à faire en message-command)");
	}

	private readonly NAMES_CONSTANTS = {
		SUB_COMMAND_GROUPS: {
			MESSAGE: {
				NAME: 'message',
				SUB_COMMANDS: {
					CREATE: {
						NAME: 'create',
						OPTIONS: {
							CHANNEL: 'channel',
							MESSAGE_CONTENT: 'message_content'
						}
					},
					UPDATE: {
						NAME: 'update',
						OPTIONS: {
							MESSAGE_CONTENT: 'message_content'
						}
					},
					REMOVE: {
						NAME: 'remove'
					}
				}
			},
			ROLE: {
				NAME: 'role',
				SUB_COMMANDS: {
					ADD: {
						NAME: 'add',
						OPTIONS: {
							ROLE: 'role',
							POSITION: 'position'
						}
					},
					REMOVE: {
						NAME: 'remove',
						OPTIONS: {
							ROLE: 'role'
						}
					}
				}
			}
		}
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription('never displayed description')
					.addSubcommandGroup((builder) =>
						builder //
							.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.NAME)
							.setDescription('never displayed description')
							.addSubcommand((builder) =>
								builder //
									.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.CREATE.NAME)
									.setDescription("Crée le message d'attribution des rôles")
									.addChannelOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.CREATE.OPTIONS.CHANNEL)
											.setDescription('The channel where the attribution message will be.')
											.setRequired(true)
									)
									.addStringOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.CREATE.OPTIONS.MESSAGE_CONTENT)
											.setDescription('The content of the bot message if needed.')
									)
							)
							.addSubcommand((builder) =>
								builder //
									.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.UPDATE.NAME)
									.setDescription("Change le texte du message d'attribution des rôles")
									.addStringOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.UPDATE.OPTIONS.MESSAGE_CONTENT)
											.setDescription('Le nouveau contenu du message')
											.setRequired(true)
									)
							)
							.addSubcommand((builder) =>
								builder //
									.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.REMOVE.NAME)
									.setDescription("Supprime le message d'attribution des rôles")
							)
					)
					.addSubcommandGroup((builder) =>
						builder //
							.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.NAME)
							.setDescription('never displayed description')
							.addSubcommand((builder) =>
								builder //
									.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.ADD.NAME)
									.setDescription("Ajoute un rôle au message d'attribution des rôles")
									.addRoleOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.ADD.OPTIONS.ROLE)
											.setDescription('Le rôle à ajouter')
											.setRequired(true)
									)
									.addIntegerOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.ADD.OPTIONS.POSITION)
											.setDescription(
												`La ligne à laquelle ajouter le role (max ${MessageLimits.MaximumActionRows} lignes et max ${InteractionLimits.MaximumButtonsPerActionRow} par lignes)`
											)
											.setMinValue(1)
											.setMaxValue(InteractionLimits.MaximumButtonsPerActionRow)
											.setRequired(true)
									)
							)
							.addSubcommand((builder) =>
								builder //
									.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.REMOVE.NAME)
									.setDescription("Retire un rôle au message d'attribution des rôles")
									.addRoleOption((option) =>
										option //
											.setName(this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.REMOVE.OPTIONS.ROLE)
											.setDescription('Le rôle à retirer')
											.setRequired(true)
									)
							)
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['952343321479626812'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		if (interaction.options.getSubcommandGroup(false) === this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.NAME) {
			return this.chatInputRunRole(interaction);
		} else if (interaction.options.getSubcommandGroup(false) === this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.NAME) {
			return this.chatInputRunMessage(interaction);
		} else {
			this.container.logger.error('Interaction with a subcommand group (' + interaction.options.getSubcommandGroup(false) + ')', interaction);
			return interaction.reply('Erreur inconnue !');
		}
	}

	public async chatInputRunMessage(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE;
		if (interaction.options.getSubcommand(false) === NAMES_CONSTANTS.SUB_COMMANDS.CREATE.NAME) {
			return this.chatInputRunMessageCreate(interaction);
		} else if (interaction.options.getSubcommand(false) === NAMES_CONSTANTS.SUB_COMMANDS.UPDATE.NAME) {
			return this.chatInputRunMessageUpdate(interaction);
		} else if (interaction.options.getSubcommand(false) === NAMES_CONSTANTS.SUB_COMMANDS.REMOVE.NAME) {
			return this.chatInputRunMessageRemove(interaction);
		} else {
			this.container.logger.error('Interaction with a subcommand (' + interaction.options.getSubcommand(false) + ')', interaction);
			return interaction.reply('Erreur inconnue !');
		}
	}
	public async chatInputRunMessageCreate(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.CREATE;

		await interaction.deferReply();

		const attributionChannel = interaction.options.getChannel(NAMES_CONSTANTS.OPTIONS.CHANNEL, true);
		const attributionMessageContent = interaction.options.getString(NAMES_CONSTANTS.OPTIONS.MESSAGE_CONTENT);

		if (attributionMessageContent !== null && attributionMessageContent.length > MessageLimits.MaximumLength) {
			return interaction.editReply(
				`Erreur: les bots ne peuvent envoyer des messages que de ${MessageLimits.MaximumLength} caractères et le tiens en fait ${
					attributionMessageContent.length
				} (c'est à dire ${attributionMessageContent.length - MessageLimits.MaximumLength} de trop).`
			);
		}

		if (attributionChannel.type !== 'GUILD_TEXT') {
			return interaction.editReply(`Error : given channel (${attributionChannel}) is not a text channel!`);
		}

		try {
			const attributionMessage = await this.fetchRoleAttributionMessage(interaction.guild as Guild);

			return interaction.editReply(`Error: attribution message already exists (here: <${attributionMessage.url}>)`);
		} catch (error) {
			if (error instanceof AttributionMessageNotFound) {
				const newAttributionMessage = await attributionChannel.send(attributionMessageContent ?? '_ _');
				await this.container.configurator.set(SettingsEnum.ROLE_ATTRIBUTION_MESSAGE_ID, newAttributionMessage.id);
				await this.container.configurator.set(SettingsEnum.ROLE_ATTRIBUTION_CHANNEL_ID, newAttributionMessage.channelId);

				return interaction.editReply(`Attribution message successfully created (here: <${newAttributionMessage.url}>)`);
			} else {
				this.container.logger.error(error);
				return interaction.reply('Erreur inconnue !');
			}
		}
	}
	public async chatInputRunMessageUpdate(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.MESSAGE.SUB_COMMANDS.UPDATE;

		await interaction.deferReply({ ephemeral: true });

		try {
			const attributionMessage = await this.fetchRoleAttributionMessage(interaction.guild as Guild);

			const attributionMessageContent = interaction.options.getString(NAMES_CONSTANTS.OPTIONS.MESSAGE_CONTENT, true);

			if (attributionMessageContent.length > MessageLimits.MaximumLength) {
				return interaction.editReply(
					`Erreur: les bots ne peuvent envoyer des messages que de ${MessageLimits.MaximumLength} caractères et le tiens en fait ${
						attributionMessageContent.length
					} (c'est à dire ${attributionMessageContent.length - MessageLimits.MaximumLength} de trop).`
				);
			}

			await attributionMessage.edit(attributionMessageContent);

			return interaction.editReply('Attribution message successfully updated');
		} catch (error) {
			if (error instanceof AttributionMessageNotFound) {
				return interaction.editReply("Can't update : " + error.message);
			} else {
				this.container.logger.error(error);
				return interaction.reply('Erreur inconnue !');
			}
		}
	}
	public async chatInputRunMessageRemove(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });

		try {
			const attributionMessage = await this.fetchRoleAttributionMessage(interaction.guild as Guild);

			await attributionMessage.delete();

			return interaction.editReply('Attribution message successfully removed');
		} catch (error) {
			if (error instanceof AttributionMessageNotFound) {
				return interaction.editReply("Can't remove : " + error.message);
			} else {
				this.container.logger.error(error);
				return interaction.reply('Erreur inconnue !');
			}
		}
	}

	public async chatInputRunRole(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE;
		if (interaction.options.getSubcommand(false) === NAMES_CONSTANTS.SUB_COMMANDS.ADD.NAME) {
			return this.chatInputRunRoleAdd(interaction);
		} else if (interaction.options.getSubcommand(false) === NAMES_CONSTANTS.SUB_COMMANDS.REMOVE.NAME) {
			return this.chatInputRunRoleRemove(interaction);
		} else {
			this.container.logger.error('Interaction with a subcommand (' + interaction.options.getSubcommand(false) + ')', interaction);
			return interaction.reply('Erreur inconnue !');
		}
	}
	public async chatInputRunRoleAdd(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.ADD;

		await interaction.deferReply({ ephemeral: true });

		try {
			const attributionMessage = await this.fetchRoleAttributionMessage(interaction.guild as Guild);

			const roleToAdd = interaction.options.getRole(NAMES_CONSTANTS.OPTIONS.ROLE, true);
			const rowPosition = interaction.options.getInteger(NAMES_CONSTANTS.OPTIONS.POSITION, true);

			if (
				attributionMessage.components.find((row) =>
					row.components.find(
						(component) => this.getRoleAttributionButtonHandler().parseDataFromCustomId(component.customId) === roleToAdd.id
					)
				)
			) {
				return interaction.editReply(`Error: Role ${roleToAdd} is already in the message`);
			}

			if (rowPosition > attributionMessage.components.length + 1) {
				return interaction.editReply(
					`Error: add a row n°${attributionMessage.components.length + 1} before trying to add a row n°${rowPosition}.`
				);
			}

			if (rowPosition === attributionMessage.components.length + 1) {
				attributionMessage.components.push(new MessageActionRow().addComponents(this.buildButtonFromRole(roleToAdd)));
			} else if (rowPosition <= attributionMessage.components.length) {
				const existingRow = attributionMessage.components[rowPosition - 1];
				if (existingRow.components.length === InteractionLimits.MaximumButtonsPerActionRow) {
					return interaction.editReply(
						`Error: the row n°${rowPosition} has already ${InteractionLimits.MaximumButtonsPerActionRow} buttons. Try adding another row.`
					);
				}
				existingRow.addComponents(this.buildButtonFromRole(roleToAdd));
				attributionMessage.components[rowPosition - 1] = existingRow;
			}

			await attributionMessage.edit({ components: attributionMessage.components });

			return interaction.editReply({ content: 'Role button successfully added' });
		} catch (error) {
			if (error instanceof AttributionMessageNotFound) {
				return interaction.editReply("Can't add role button : " + error.message);
			} else {
				this.container.logger.error(error);
				return interaction.reply('Erreur inconnue !');
			}
		}
	}

	public async chatInputRunRoleRemove(interaction: CommandInteraction) {
		const NAMES_CONSTANTS = this.NAMES_CONSTANTS.SUB_COMMAND_GROUPS.ROLE.SUB_COMMANDS.REMOVE;

		await interaction.deferReply({ ephemeral: true });

		try {
			const attributionMessage = await this.fetchRoleAttributionMessage(interaction.guild as Guild);

			const roleToRemove = interaction.options.getRole(NAMES_CONSTANTS.OPTIONS.ROLE, true);

			const rowWithTheRoleIndex = attributionMessage.components.findIndex((row) =>
				row.components.find(
					(component) => this.getRoleAttributionButtonHandler().parseDataFromCustomId(component.customId) === roleToRemove.id
				)
			);

			if (rowWithTheRoleIndex === -1) {
				return interaction.editReply(`Error: Role ${roleToRemove} is not in the message`);
			}
			attributionMessage.components[rowWithTheRoleIndex].components = attributionMessage.components[rowWithTheRoleIndex].components.filter(
				(component) => this.getRoleAttributionButtonHandler().parseDataFromCustomId(component.customId) !== roleToRemove.id
			);

			if (attributionMessage.components[rowWithTheRoleIndex].components.length === 0) {
				// it was the last button of the row, remove the row too
				attributionMessage.components.splice(rowWithTheRoleIndex, 1);
			}

			await attributionMessage.edit({ components: attributionMessage.components });

			return interaction.editReply({ content: 'Role button successfully removed' });
		} catch (error) {
			if (error instanceof AttributionMessageNotFound) {
				return interaction.editReply("Can't remove role button : " + error.message);
			} else {
				this.container.logger.error(error);
				return interaction.reply('Erreur inconnue !');
			}
		}
	}

	private buildButtonFromRole(roleToAdd: Role | APIRole) {
		return new MessageButton() //
			.setCustomId(this.getRoleAttributionButtonHandler().buildCustomIdFromData(roleToAdd.id)) //
			.setLabel(roleToAdd.name) //
			.setStyle('SECONDARY');
	}

	private getRoleAttributionButtonHandler() {
		return this.container.stores
			.get('button-handlers')
			.find((handler) => handler.identifier === RoleAttributionButtonHandler.getIdentifier()) as RoleAttributionButtonHandler;
	}

	private async fetchRoleAttributionMessage(guild: Guild) {
		try {
			const attributionChannelId = await this.container.configurator.get(SettingsEnum.ROLE_ATTRIBUTION_CHANNEL_ID);
			const attributionMessageId = await this.container.configurator.get(SettingsEnum.ROLE_ATTRIBUTION_MESSAGE_ID);
			const attributionChannel = await guild.channels.fetch(attributionChannelId);

			if (attributionChannel === null) {
				throw new AttributionMessageNotFound(
					`Stored channel (\`${attributionChannelId}\`) is not found. You must recreate the attribution message with \`/role-attribution message create\``
				);
			}

			if (!attributionChannel.isText()) {
				throw new AttributionMessageNotFound(
					`Stored channel (${attributionChannel}) is not a text channel. You must recreate the attribution message with \`/role-attribution message create\``
				);
			}

			const attributionMessage = await attributionChannel.messages.fetch(attributionMessageId);

			if (attributionMessage === null) {
				await this.container.configurator.unset(SettingsEnum.ROLE_ATTRIBUTION_MESSAGE_ID);
				await this.container.configurator.unset(SettingsEnum.ROLE_ATTRIBUTION_CHANNEL_ID);
				throw new AttributionMessageNotFound(
					'Attribution message id is in database, but the message has been manually removed in Discord. Anyway, you must recreate it!'
				);
			}

			return attributionMessage;
		} catch (error) {
			if (error instanceof SettingsNotFoundError) {
				throw new AttributionMessageNotFound(
					'Attribution message does not exist, you must create it with `/role-attribution message create`'
				);
			}
			throw error;
		}
	}
}

class AttributionMessageNotFound extends Error {}
