import { ApplyOptions } from '@sapphire/decorators';
import { Args, ArgumentError, Command, CommandOptions, Identifiers } from '@sapphire/framework';
import { Constants, Guild, GuildMember, Message } from 'discord.js';
import { SettingsNotFoundError } from '../../lib/settings/settings-not-found.error';
import { SettingsEnum } from '../../lib/settings/settings.enum';
import { isDiscordColorResolvable } from '../../lib/utils';

@ApplyOptions<CommandOptions>({
	aliases: ['custom-role-add'],
	description: "Ajoute un role custom à l'utilisateur de la commande (choix du nom du rôle et de sa couleur)",
	preconditions: ['OwnerOnly', 'AdminOnly', 'GuildTextOnly']
})
export class UserCommand extends Command {
	public constructor(context: Command.Context, options: Command.Options) {
		super(context, { ...options });
	}

	public async messageRun(commandMessage: Message, args: Args) {
		if (!commandMessage.channel.isText() || !commandMessage.guild) {
			return commandMessage.reply('This command can only be used in guild text channels.');
		}

		const roleNameArg = await args.pick('string').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return undefined;
			} else {
				throw error;
			}
		});

		if (roleNameArg === undefined) {
			return commandMessage.reply('Il manquerait pas le nom du rôle souhaité par hasard :thinking: ?');
		}

		const roleColorArg = await args.pick('string').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return null;
			} else {
				throw error;
			}
		});

		try {
			await this.doTheThing(roleNameArg, roleColorArg, commandMessage.guild!, commandMessage.member!);
			return commandMessage.react('✅');
		} catch (error) {
			return commandMessage.reply('Error : ' + error);
		}
	}

	private async doTheThing(roleNameArg: string, roleColorArg: string | null, guild: Guild, member: GuildMember) {
		if (roleColorArg !== null && !isDiscordColorResolvable(roleColorArg)) {
			throw new Error(
				"La couleur demandée n'est pas valide petit chenapan !" +
					'\nsoit un code hexa comme `#9b59b6`, soit une des couleur de cette liste :' +
					'\n`' +
					Object.keys(Constants.Colors).join('`, `') +
					'`'
			);
		}

		const customRolePositionBeforeId = await this.container.configurator.get(SettingsEnum.APRIL_FOOL_ROLES_POSITION);

		const targetRoleForColor = await guild.roles.fetch(customRolePositionBeforeId);

		if (!targetRoleForColor) {
			throw new Error(
				`Can't find a role with id ${customRolePositionBeforeId}. Check settings with set command (the setting is ${SettingsEnum.APRIL_FOOL_ROLES_POSITION})`
			);
		}
		const roleCreated = await guild.roles.create({
			name: roleNameArg,
			color: roleColorArg ?? 'DEFAULT',
			position: targetRoleForColor.position
		});

		if (!roleCreated) {
			throw new Error('Error while creating the role.');
		}

		try {
			const alreadyCreatedRoles = await this.container.configurator.get(SettingsEnum.APRIL_FOOL_ROLES);
			await this.container.configurator.set(SettingsEnum.APRIL_FOOL_ROLES, alreadyCreatedRoles + ', ' + roleCreated.id);
		} catch (error) {
			if (error instanceof SettingsNotFoundError) {
				await this.container.configurator.set(SettingsEnum.APRIL_FOOL_ROLES, roleCreated.id);
			} else {
				throw error;
			}
		}

		const getinfoChannelCategoryId = await this.container.configurator.get(SettingsEnum.APRIL_FOOL_DOOMED_CATEGORY_ID);
		const getinfoChannelCategory = await guild.channels.fetch(getinfoChannelCategoryId);

		if (getinfoChannelCategory === null) {
			throw new Error(`Impossible de trouver la categorie ||maudite|| configurée (id: ${getinfoChannelCategoryId})`);
		}

		await getinfoChannelCategory.permissionOverwrites.create(roleCreated, {
			VIEW_CHANNEL: true
		});

		await member.roles.add(roleCreated);
	}
}
