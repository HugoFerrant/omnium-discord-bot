import { ApplyOptions } from '@sapphire/decorators';
import { PaginatedMessage } from '@sapphire/discord.js-utilities';
import { ApplicationCommandRegistry, Command, CommandOptions, RegisterBehavior } from '@sapphire/framework';
import { Collection, CommandInteraction, Message } from 'discord.js';
import { envParseArray } from '../../lib/env-parser';
import { sendLoadingMessage } from '../../lib/utils';

@ApplyOptions<CommandOptions>({
	description: 'Affiche une aide résumant toutes les commandes disponibles.'
})
export class UserCommand extends Command {
	private readonly OPTIONS_NAMES = {
		REQUESTED_COMMAND: 'requested_command',
		REQUESTED_CATEGORY: 'requested_category'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		const commandsStore = this.container.stores.get('commands');
		const commandOptions = commandsStore.map<[string, string]>((command) => [command.name, command.name]);
		const categoriesOptions = commandsStore.categories.map<[string, string]>((category) => [category, category]);

		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.REQUESTED_COMMAND)
							.setDescription('choisir une commande en particulière')
							.setChoices(commandOptions)
					)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.REQUESTED_CATEGORY)
							.setDescription('choisir une categorie en particulière')
							.setChoices(categoriesOptions)
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015355322245120'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async messageRun(message: Message) {
		const paginatedMessage = this.doTheThing(null, null);

		const response = await sendLoadingMessage(message);

		await paginatedMessage.run(response, message.author);
		return response;
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });

		const requestedCommand = interaction.options.getString(this.OPTIONS_NAMES.REQUESTED_COMMAND);
		const requestedCategory = interaction.options.getString(this.OPTIONS_NAMES.REQUESTED_CATEGORY);

		if (requestedCommand && requestedCategory) {
			return interaction.editReply("Sorry, but you can't use both of these options at the same time.");
		}

		const paginatedMessage = this.doTheThing(requestedCommand, requestedCategory);

		return paginatedMessage.run(interaction);
	}

	private doTheThing(requestedCommand: string | null, requestedCategory: string | null) {
		const commandsStore = this.container.stores.get('commands');
		const commandsByCategory = new Collection<string, Command[]>();
		for (const [, command] of commandsStore) {
			if ((requestedCategory && command.category !== requestedCategory) || (requestedCommand && command.name !== requestedCommand)) {
				continue;
			}
			const commandCategory = command.category ?? '(à trier)';
			const alreadyPresentCommands = commandsByCategory.get(commandCategory) ?? [];
			alreadyPresentCommands.push(command);
			commandsByCategory.set(commandCategory, alreadyPresentCommands);
		}

		const paginatedMessage = new PaginatedMessage().setSelectMenuOptions((pageIndex) => ({
			label: `${commandsByCategory.keyAt(pageIndex - 1)!} (${commandsByCategory.at(pageIndex - 1)?.length} commande${
				(commandsByCategory.at(pageIndex - 1)?.length ?? 0) > 1 ? 's' : ''
			})`,
			description: '\n Page ' + pageIndex
		}));

		for (const [category, commands] of commandsByCategory) {
			paginatedMessage.addPageEmbed((embed) =>
				embed //
					.setTitle(`Catégorie : ${category}`)
					.setDescription(commands.map((command) => `**${command.name} :** ${command.description}`).join('\n'))
					.setFooter(category ? { text: ' ' + category } : null)
			);
		}

		return paginatedMessage;
	}
}
