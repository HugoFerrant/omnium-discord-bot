import { ApplyOptions } from '@sapphire/decorators';
import { ApplicationCommandRegistry, Args, ArgumentError, Command, CommandOptions, Identifiers, RegisterBehavior } from '@sapphire/framework';
import type { CommandInteraction, Message } from 'discord.js';
import { envParseArray } from '../../../lib/env-parser';
import { SettingsEnum } from '../../../lib/settings/settings.enum';
import { isSettingsEnum } from '../../../lib/settings/settings.utils';

@ApplyOptions<CommandOptions>({
	aliases: ['config-set', 'settings-set'],
	description: 'Permet de modifier la configuration du bot',
	preconditions: ['GuildOnly', ['OwnerOnly', 'AdminOnly']]
})
export class UserCommand extends Command {
	private readonly OPTIONS_NAMES = {
		SETTING_NAME: 'setting_name',
		SETTING_VALUE: 'setting_value'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.SETTING_NAME)
							.setDescription('Nom du setting')
							.setChoices(Object.entries(SettingsEnum))
							.setRequired(true)
					)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.SETTING_VALUE)
							.setDescription('Valeur souhaitée')
							.setRequired(true)
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015352281366538'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });
		const requestedSettingName = interaction.options.getString(this.OPTIONS_NAMES.SETTING_NAME, true) as SettingsEnum; //thx to setChoices
		const requestedSettingValue = interaction.options.getString(this.OPTIONS_NAMES.SETTING_VALUE, true);

		try {
			await this.doTheThing(requestedSettingName, requestedSettingValue);
			return interaction.editReply('Done ✅ !');
		} catch (error) {
			if (error instanceof Error) {
				return interaction.editReply(error.message);
			}
			throw error;
		}
	}

	public override async messageRun(message: Message, args: Args) {
		if (args.finished) {
			return message.reply(`Vous devez spécifier le nom du setting à modifier, ainsi que sa valeur souhaitée.`);
		}

		const requestedSettingName = await args.pick('string');

		if (!isSettingsEnum(requestedSettingName)) {
			return message.reply(
				`Le nom du setting à modifier doit faire partie des settings existants (\`${Object.values(SettingsEnum).join('`, `')}\`).`
			);
		}

		const requestedSettingValue = await args.pick('string').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return undefined;
			} else {
				throw error;
			}
		});

		if (requestedSettingValue === undefined) {
			return message.reply('Vous devez spécifier la valeur souhaitée pour le setting à modifier.');
		}

		try {
			await this.doTheThing(requestedSettingName, requestedSettingValue);
			return message.react('✅');
		} catch (error) {
			if (error instanceof Error) {
				return message.reply(error.message);
			}
			throw error;
		}
	}

	private async doTheThing(requestedSettingName: SettingsEnum, requestedSettingValue: string) {
		await this.container.configurator.set(requestedSettingName, requestedSettingValue);
	}
}
