import type { Settings } from '@prisma/client'; //todo: re-export
import { ApplyOptions } from '@sapphire/decorators';
import { ApplicationCommandRegistry, Args, ArgumentError, Command, CommandOptions, Identifiers, RegisterBehavior } from '@sapphire/framework';
import type { CommandInteraction, Message } from 'discord.js';
import { envParseArray } from '../../../lib/env-parser';
import { SettingsEnum } from '../../../lib/settings/settings.enum';
import { isSettingsEnum } from '../../../lib/settings/settings.utils';
import { dateToUnixTimestamp } from '../../../lib/utils';

@ApplyOptions<CommandOptions>({
	aliases: ['config-get', 'settings-get'],
	description: "Permet d'afficher la configuration du bot",
	preconditions: ['GuildOnly', ['OwnerOnly', 'AdminOnly']]
})
export class UserCommand extends Command {
	private readonly OPTIONS_NAMES = {
		SETTING_NAME: 'setting_name'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.SETTING_NAME)
							.setDescription('Nom du setting')
							.setChoices(Object.entries(SettingsEnum))
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['974015353720037406'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });
		const requestedSettingName = interaction.options.getString(this.OPTIONS_NAMES.SETTING_NAME);

		try {
			return interaction.editReply(await this.doTheThing(requestedSettingName));
		} catch (error) {
			if (error instanceof Error) {
				return interaction.editReply(error.message);
			}
			throw error;
		}
	}

	public async messageRun(message: Message, args: Args) {
		const requestedSettingName = await args.pick('string').catch((error: ArgumentError) => {
			if (error.identifier === Identifiers.ArgsMissing) {
				return null;
			} else {
				throw error;
			}
		});

		try {
			return message.react(await this.doTheThing(requestedSettingName));
		} catch (error) {
			if (error instanceof Error) {
				return message.reply(error.message);
			}
			throw error;
		}
	}

	private async doTheThing(requestedSettingName: string | null) {
		if (requestedSettingName === null) {
			const allSettings = await this.container.configurator.getAllSettings();

			if (allSettings.length === 0) {
				return "Il n'y a actuellement aucun settings";
			}

			return allSettings.map((setting) => this.displaySetting(setting)).join('\n');
		}

		if (!isSettingsEnum(requestedSettingName)) {
			throw new Error(
				`Le nom du setting à modifier doit faire partie des settings existants (\`${Object.values(SettingsEnum).join('`, `')}\`).`
			);
		}

		const requestedSetting = await this.container.configurator.getOneSetting(requestedSettingName);

		return this.displaySetting(requestedSetting);
	}

	private displaySetting(requestedSetting: Settings) {
		return `\`${requestedSetting.name}\` :\t\`${requestedSetting.value}\` (créé le <t:${dateToUnixTimestamp(
			requestedSetting.createdAt
		)}>, mis à jour le <t:${dateToUnixTimestamp(requestedSetting.updatedAt)}>)`;
	}
}
