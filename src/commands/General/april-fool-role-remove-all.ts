import { ApplyOptions } from '@sapphire/decorators';
import { Command, CommandOptions } from '@sapphire/framework';
import type { Guild, Message } from 'discord.js';
import { SettingsNotFoundError } from '../../lib/settings/settings-not-found.error';
import { SettingsEnum } from '../../lib/settings/settings.enum';

@ApplyOptions<CommandOptions>({
	description: 'Supprime tous les roles custom créés grace à la commande role-add',
	preconditions: ['OwnerOnly', 'AdminOnly', 'GuildOnly']
})
export class UserCommand extends Command {
	public constructor(context: Command.Context, options: Command.Options) {
		super(context, { ...options });
	}

	public async messageRun(message: Message) {
		if (message.guild === null) {
			return message.reply('Cette commande est seulement utilisable dans les guildes');
		}

		try {
			await this.doTheThing(message.guild);
		} catch (error) {
			return message.reply((error as Error).message);
		}
		return message.react('✅');
	}

	private async doTheThing(guild: Guild) {
		try {
			const alreadyCreatedRoles = await this.container.configurator.get(SettingsEnum.APRIL_FOOL_ROLES);
			for (const role of alreadyCreatedRoles.split(', ')) {
				const roleFound = await guild.roles.fetch(role);
				if (roleFound === null) {
					return this.container.logger.info(`Role ${role} was not found when trying to delete it.`);
				}
				await roleFound.delete();
			}

			await this.container.configurator.unset(SettingsEnum.APRIL_FOOL_ROLES);
		} catch (error) {
			if (error instanceof SettingsNotFoundError) {
				throw new Error("Il n'y a pas de roles customs créés actuellement");
			}
			throw error;
		}
	}
}
