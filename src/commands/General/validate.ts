import { SettingsEnum } from '@prisma/client';
import { ApplyOptions } from '@sapphire/decorators';
import { ApplicationCommandRegistry, Command, CommandOptions, RegisterBehavior } from '@sapphire/framework';
import { CommandInteraction, ContextMenuInteraction, GuildMember, UserContextMenuInteraction } from 'discord.js';
import { envParseArray } from '../../lib/env-parser';
import { validateMember } from '../../lib/validate-member';
import { ApplicationCommandType } from 'discord-api-types/v9';

@ApplyOptions<CommandOptions>({
	description: "Valide un utilisateur (pour éviter l'attente de 10mins)."
})
export class UserCommand extends Command {
	private readonly OPTIONS_NAMES = {
		TARGET_MEMBER_ID: 'target_member'
	};

	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			(builder) =>
				builder //
					.setName(this.name)
					.setDescription(this.description)
					.addStringOption((option) =>
						option //
							.setName(this.OPTIONS_NAMES.TARGET_MEMBER_ID)
							.setDescription('choisir un utilisateur')
							.setRequired(true)
							.setAutocomplete(true)
					),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['982438187957899344'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
		registry.registerContextMenuCommand(
			(builder) =>
				builder //
					.setName(this.name + ' via user')
					.setType(ApplicationCommandType.User),
			{
				guildIds: envParseArray('COMMAND_GUILD_IDS', []),
				idHints: ['982686350056763452'],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		await interaction.deferReply({ ephemeral: true });

		const targetMember = await interaction.guild!.members.fetch(interaction.options.getString(this.OPTIONS_NAMES.TARGET_MEMBER_ID, true));

		if (!(targetMember instanceof GuildMember)) {
			return interaction.editReply(`${targetMember} n'est pas un utilisateur correct`);
		}

		return this.doTheThing(targetMember, interaction);
	}

	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		await interaction.deferReply({ ephemeral: true });

		if (!interaction.isUserContextMenu()) {
			this.container.logger.error('Context menu interaction with an unhandled type (' + interaction.targetType + ')', interaction);
			return interaction.editReply('Erreur inconnue !');
		}

		const targetMember = interaction.targetMember;

		if (!(targetMember instanceof GuildMember)) {
			return interaction.editReply(`${targetMember} n'est pas un utilisateur correct`);
		}

		return this.doTheThing(targetMember, interaction);
	}

	private async doTheThing(targetMember: GuildMember, interaction: CommandInteraction | UserContextMenuInteraction) {
		const logsChannelId = await this.container.configurator.get(SettingsEnum.LOGS_CHANNEL_ID);
		const logsChannel = await targetMember.guild.channels.fetch(logsChannelId);

		if (logsChannel === null || !logsChannel.isText()) {
			return this.container.logger.error(
				`Erreur lors de la récupération du channel de logs. L'id enregistré est \`${logsChannelId}\`. Son settings est \`${SettingsEnum.LOGS_CHANNEL_ID}\``
			);
		}
		logsChannel.send(`${interaction.member} validated ${targetMember}.`);

		validateMember(targetMember, this.container, logsChannel);

		return interaction.editReply('Done ✅ !');
	}
}
