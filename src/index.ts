import './lib/setup';
import { CustomClient } from './customClient';

const client = new CustomClient();

const main = async () => {
	try {
		client.logger.info('Logging in');
		await client.login();
		client.logger.info(`Logged in as ${client.user?.tag}! (running v${process.env.npm_package_version})`);
	} catch (error) {
		client.logger.fatal(error);
		client.destroy();
		process.exit(1);
	}
};

main();
