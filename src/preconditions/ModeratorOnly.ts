import { Precondition } from '@sapphire/framework';
import { CommandInteraction, ContextMenuInteraction, Message, Permissions } from 'discord.js';

// MODERATE_MEMBERS aka timeout
export class UserPrecondition extends Precondition {
	public override async messageRun(message: Message) {
		if (message.guild && message.member && message.member.permissions.has(Permissions.FLAGS.MODERATE_MEMBERS)) return this.ok();
		return this.error({ message: 'This command can only be used by server moderators.' });
	}
	public override async chatInputRun(interaction: CommandInteraction) {
		if (interaction.guild && interaction.memberPermissions && interaction.memberPermissions.has(Permissions.FLAGS.MODERATE_MEMBERS))
			return this.ok();
		return this.error({ message: 'This command can only be used by server moderators.' });
	}
	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		if (interaction.guild && interaction.memberPermissions && interaction.memberPermissions.has(Permissions.FLAGS.MODERATE_MEMBERS))
			return this.ok();
		return this.error({ message: 'This command can only be used by server moderators.' });
	}
}

declare module '@sapphire/framework' {
	interface Preconditions {
		ModeratorOnly: never;
	}
}
