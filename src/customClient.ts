import { container, LogLevel, SapphireClient } from '@sapphire/framework';
import { createDatabaseConnection } from './lib/database';
import type { PrismaClient } from '@prisma/client';
import { envParseString } from './lib/env';
import { Intents, Message } from 'discord.js';
import { ButtonHandlersStore } from './lib/buttons-handling/button-handlers.store';
import { Configurator } from './lib/settings/configurator';
import { SettingsEnum } from './lib/settings/settings.enum';
import { isGuildBasedChannel } from '@sapphire/discord.js-utilities';
import { SettingsNotFoundError } from './lib/settings/settings-not-found.error';

export class CustomClient extends SapphireClient {
	public dev = envParseString('NODE_ENV', 'production') !== 'production';

	public constructor() {
		super({
			defaultPrefix: 'a!',
			regexPrefix: /^(hey +)?bot[,! ]/i,
			caseInsensitiveCommands: true,
			caseInsensitivePrefixes: true,
			typing: true,
			logger: {
				level: envParseString('NODE_ENV', 'production') === 'production' ? LogLevel.Info : LogLevel.Debug,
				depth: 3
			},
			shards: 'auto',
			intents: [
				Intents.FLAGS.GUILDS,
				Intents.FLAGS.GUILD_MEMBERS,
				Intents.FLAGS.GUILD_BANS,
				Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
				Intents.FLAGS.GUILD_VOICE_STATES,
				Intents.FLAGS.GUILD_MESSAGES,
				Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
				Intents.FLAGS.DIRECT_MESSAGES,
				Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
				Intents.FLAGS.DIRECT_MESSAGE_TYPING
			],
			loadMessageCommandListeners: true
		});
	}

	public override async login(token?: string) {
		container.database = createDatabaseConnection();
		container.configurator = new Configurator(container.database);

		container.stores.register(new ButtonHandlersStore());

		return super.login(token);
	}

	public override async destroy() {
		await container.database.$disconnect();
		return super.destroy();
	}

	public override fetchPrefix = async (message: Message) => {
		if (isGuildBasedChannel(message.channel)) {
			try {
				return await container.configurator.get(SettingsEnum.PREFIX);
			} catch (error) {
				if (!(error instanceof SettingsNotFoundError)) {
					this.logger.error('Error while fetching prefix, we(lle use the default one. Here is the error : ' + error);
				}
			}
		}

		return [this.options.defaultPrefix, ''] as readonly string[];
	};
}

declare module '@sapphire/pieces' {
	interface Container {
		database: PrismaClient;
		configurator: Configurator;
	}
}

declare module '@sapphire/framework' {
	interface SapphireClient {
		dev: boolean;
	}
}
