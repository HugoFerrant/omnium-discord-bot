/*
  Warnings:

  - The values [NEW_MEMBER_WELCOME_WAIT_SECONDS] on the enum `SettingsEnum` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "SettingsEnum_new" AS ENUM ('ROLE_ATTRIBUTION_MESSAGE_ID', 'ROLE_ATTRIBUTION_CHANNEL_ID', 'WELCOME_CHANNEL_ID', 'PREFIX', 'APRIL_FOOL_ROLES', 'APRIL_FOOL_ROLES_POSITION', 'APRIL_FOOL_DOOMED_CATEGORY_ID', 'LOGS_CHANNEL_ID', 'VALIDATED_ROLE_ID');
ALTER TABLE "Settings" ALTER COLUMN "name" TYPE "SettingsEnum_new" USING ("name"::text::"SettingsEnum_new");
ALTER TYPE "SettingsEnum" RENAME TO "SettingsEnum_old";
ALTER TYPE "SettingsEnum_new" RENAME TO "SettingsEnum";
DROP TYPE "SettingsEnum_old";
COMMIT;
