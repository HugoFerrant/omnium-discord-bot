const configurator = require('../helpers/configurator');

module.exports = {
	name: 'role-custom-remove-all',
	description: "Supprime tous les roles custom créés grace à la commande role-add",
	guildOnly: true,
    permissionsRequired: ['KICK_MEMBERS'],
	async execute(commandMessage, args) {
		const alreadyCreatedRoles = await configurator.get('roles_1er_avril');

		if (alreadyCreatedRoles === undefined || alreadyCreatedRoles === '') {
			return commandMessage.reply("Il n'y a pas de roles customs créés actuellement");
		}
	
		for(role of alreadyCreatedRoles.split(', ')) {
			if (role === '' || role === 'undefined') { //just in case
				continue;
			}

			await (await commandMessage.guild.roles.fetch(role))?.delete();
		}

		await configurator.set('roles_1er_avril', '');

		return commandMessage.react('✅');
	},
};