const configurator = require('../helpers/configurator');
const { Constants } = require('discord.js');
const index = require('../index');


const isDiscordColor = (color) => {
	return /^#(?:[0-9a-fA-F]{3}){1,2}$/.test(color) || Object.keys(Constants.Colors).includes(color);
}

module.exports = {
	name: 'role-add',
	aliases: ['role-custom', 'add-role'],
	description: "Ajoute un role custom à l'utilisateur de la commande (choix du nom du rôle et de sa couleur)",
	guildOnly: true,
	argsRequired: true,
	cooldown: 10,
	usage: 'role_name color',
    permissionsRequired: ['KICK_MEMBERS'], //just to avoid usage
	async execute(commandMessage, args) {

		const roleNameArg = args[0];
		const roleColorArg = args[1];

		if (typeof roleNameArg !== 'string') {
			return commandMessage.reply("Le rôle demandé n'est pas valide petit chenapan");
		}

		if (!isDiscordColor(roleColorArg)) {
			return commandMessage.reply(
				"La couleur demandée n'est pas valide petit chenapan !"
				+ "\nsoit un code hexa comme `#9b59b6`, soit une des couleur de cette liste :"
				+ "\n`" + Object.keys(Constants.Colors).join('`, `') + "`"
			);
		}

		const customRolePositionBeforeId = await configurator.get('custom_role_position_before');
		const targetRoleForColor = await commandMessage.guild.roles.fetch(customRolePositionBeforeId)

		if (!targetRoleForColor) {
			return commandMessage.reply(`Can't find a role with id ${customRolePositionBeforeId}. Check settings with set command (the setting is 'custom_role_position_before')`)
		}
		const roleCreated = await commandMessage.guild.roles.create({
			data: {
				name: roleNameArg,
				color: roleColorArg,
				position: targetRoleForColor.position,
			}
		});

		const alreadyCreatedRoles = await configurator.get('roles_1er_avril');
		if (alreadyCreatedRoles === undefined || alreadyCreatedRoles === '') {
			await configurator.set('roles_1er_avril', roleCreated.id)
		} else {
			await configurator.set('roles_1er_avril', alreadyCreatedRoles + ', ' + roleCreated.id)
		}


		// const getinfoChannelCategoryId = await configurator.get('getinfo_channel_category_id');
		// const getinfoChannelCategory = index.botClient.channels.cache.find(channel => channel.id === getinfoChannelCategoryId)

		// await getinfoChannelCategory.createOverwrite(roleCreated, {
		// 	VIEW_CHANNEL: true
		// })

		await commandMessage.member.roles.add(roleCreated)

		return commandMessage.react('✅');
	},
};