# Omnium server custom bot

This bot is made by [Hugo Ferrant (aka Xelodrin)](https://gitlab.com/HugoFerrant) for the custom needs of the Omnium server. It has absolutely no intention to be used in others and/or multiple servers at this time.
This repo is mainly there to backup the code, use the CD. It might stay private a long time until I feel confortable with unveiling it.

[![staging status](https://gitlab.com/HugoFerrant/omnium-discord-bot/badges/staging/pipeline.svg?key_text=Staging+Pipeline&key_width=100)](https://gitlab.com/HugoFerrant/omnium-discord-bot/-/commits/staging)

## Add the bot to the server with the following links :

Add Dev Bot : https://discord.com/api/oauth2/authorize?client_id=761691844265377844&permissions=1636114627666&scope=bot%20applications.commands
Add PTR Bot : https://discord.com/api/oauth2/authorize?client_id=755177152684621995&permissions=1636114627666&scope=bot%20applications.commands

## How to use it?

### Prerequisite

Copy the `.env.example` and rename it to `.env`, make sure to fill the Token.

### Development

Adapt the `.env` to use the dev docker-compose file.
Run `docker-compose up`. This will start the bot in watch mode and automatically run it after each save.

### Production

Just like in the development step, you have to fill/adapt in the `.env` file.
You need to run `docker-compose up --build` for the code changes to be applied.

## Found a bug ? a possible enhancement ?

Fill an issue on gitlab : https://gitlab.com/HugoFerrant/omnium-discord-bot/-/issues

## Todo list

-   [x] Reduce permissions required by the bot when installing (full admin atm!)
-   [x] Explain command usage when needed
-   [x] Add permissions requirements on commands
-   [x] Add role requirements on commands
-   [x] Add possibility to mention bot instead of prefix
-   [x] Add settings command
-   [x] Better releases management (SemVer + show version in bot status)
-   [ ] Find a way to handle bot language (or just hardcode french)
-   [ ] Unify the ways to reply to a command (reply message, react to command or remove the command ?)
-   [ ] Attribution Role : allow edition of existing role lines
-   [ ] Again better releases management (commitizen + gitlab releases)
-   [ ] Again again better releases management (custom webhook that send messages per env per deploy)
-   [ ] Better doc for contributing/dev process
-   [ ] Nettoyer ce repos pour Winzana :smile:
-   [ ] ...
